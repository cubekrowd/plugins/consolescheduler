package com.RkCraft.ConsoleScheduler;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class ConsoleScheduler extends JavaPlugin {
    Date date = new Date();
    Calendar calendar = GregorianCalendar.getInstance();

    @Override
    public void onEnable() {
        saveDefaultConfig();
        getLogger().log(Level.INFO, "Command execution will start in {0} seconds.",
                getConfig().getInt("InitialDelay"));
        initialDelay();
    }

    public void initialDelay() {
        getServer().getScheduler().runTaskLater(this, () -> {
            getLogger().info("Starting to execute commands");
            getLogger().info("-------------[ConsoleScheduler]--------------");
            startSchedule();
        }, (long) getConfig().getInt("InitialDelay"));
    }

    public void startSchedule() {
        int counter = 1;
        int started = 0;
        while (getConfig().contains("CommandSchedule.Command" + counter)) {
            getLogger().log(Level.INFO, "getConfig contains CommandSchedule.Command{0}", counter);
            if (!getConfig().contains("CommandSchedule.Command" + counter + ".After")
                    && !getConfig().getBoolean("CommandSchedule.Command" + counter + ".SpecificTime", false)) {
                getLogger().log(Level.INFO, "Command{0} does not have an After value, defaulting to 0.", counter);
                getConfig().set("CommandSchedule.Command" + counter + ".After", 0);
            }
            if (getConfig().getBoolean("CommandSchedule.Command" + counter + ".SpecificTime", false)) {
                timeTask(counter);
            } else if (getConfig().getBoolean("CommandSchedule.Command" + counter + ".Repeat")) {
                if (!getConfig().contains("CommandSchedule.Command" + counter + ".Interval")) {
                    getLogger().log(Level.INFO, "Command{0} has Repeat: true, but Interval is not set! Ignoring this command.", counter);
                } else {
                    repeatingTask(counter);
                }
            } else {
                nonrepeatingTask(counter);
            }
            ++started;
            ++counter;
        }
        getLogger().log(Level.INFO, "Attempted to put {0} commands on schedule.", started);
    }

    public void repeatingTask(final int counter) {
        getServer().getScheduler().runTaskTimer(this, () -> runCommand(counter),
                getConfig().getInt("CommandSchedule.Command" + counter + ".After", 0) * 20L,
                getConfig().getInt("CommandSchedule.Command" + counter + ".Interval") * 20L);
    }

    public void nonrepeatingTask(final int counter) {
        getServer().getScheduler().runTaskLater(this, () -> runCommand(counter),
                getConfig().getInt("CommandSchedule.Command" + counter + ".After", 0) * 20L);
    }

    public void timeTask(final int counter) {
        getServer().getScheduler().runTaskTimer(this, () -> runCommand(counter),
                getOffset(counter) * 20L, 1728000L);
    }

    public void runCommand(int counter) {
        var command = getConfig().getString("CommandSchedule.Command" + counter + ".Command");
        getServer().dispatchCommand(getServer().getConsoleSender(), command);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var desc = getDescription();
        if (args.length == 0) {
            msg(sender, ChatColor.RED + "[ConsoleScheduler] " + ChatColor.WHITE + "To see a list of commands type: /csc help", new Object[0]);
            return true;
        }
        if (!hasPerm(sender, "ConsoleScheduler.use")) {
            msg(sender, ChatColor.RED + "You don't have permission to use this command", new Object[0]);
            return true;
        }
        if (args[0].equalsIgnoreCase("reload")) {
            msg(sender, ChatColor.RED + "[ConsoleScheduler] " + ChatColor.WHITE + "Reloading...", new Object[0]);
            Bukkit.getScheduler().cancelTasks(this);
            reloadConfig();
            initialDelay();
            msg(sender, ChatColor.RED + "[ConsoleScheduler] " + ChatColor.GREEN + "Reloaded config file!", new Object[0]);
            return true;
        }
        if (args[0].equalsIgnoreCase("help")) {
            msg(sender, ChatColor.RED + "[ConsoleScheduler] " + ChatColor.WHITE + "Commands:", new Object[]{desc.getVersion()});
            msg(sender, ChatColor.WHITE + "* /csc reload", new Object[0]);
            return true;
        }
        return false;
    }

    private boolean hasPerm(CommandSender sender, String perm) {
        if (perm == null || perm.equals("")) {
            return true;
        }
        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;
        return player.hasPermission(perm);
    }

    public void msg(CommandSender sender, String msg, Object[] objects) {
        msg = MessageFormat.format(msg, objects);
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.stripColor(msg));
        } else {
            sender.sendMessage(msg);
        }
    }

    public int getOffset(int counter) {
        calendar.setTime(date);
        int time_in_seconds = calendar.get(11) * 3600 + calendar.get(12) * 60 + calendar.get(13);
        int time_wanted = getConfig().getInt("CommandSchedule.Command" + counter + ".Hour", 0) * 3600
                + getConfig().getInt("CommandSchedule.Command" + counter + ".Minute", 0) * 60
                + getConfig().getInt("CommandSchedule.Command" + counter + ".Second", 0);
        int Offset = time_wanted >= time_in_seconds ?
                time_wanted - time_in_seconds : 86400 + time_wanted - time_in_seconds;
        return Offset;
    }
}
